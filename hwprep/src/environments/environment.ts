// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyByVDDA0Ebt7oEmrL6GyP_2aLyqlksFrCk",
    authDomain: "hwprep-8d55e.firebaseapp.com",
    databaseURL: "https://hwprep-8d55e.firebaseio.com",
    projectId: "hwprep-8d55e",
    storageBucket: "hwprep-8d55e.appspot.com",
    messagingSenderId: "150430772920",
    appId: "1:150430772920:web:33a111bbd6e48b100e0e4d"
  
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
