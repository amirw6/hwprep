import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  authors: object[] = [{id:'1',name:'Kobe'}, {id:'2',name:'Nir'}, {id:'3',name:'Matan'}];

  constructor() { }


  addAuthor(newAuthor:string){
    
      this.authors.push({name:newAuthor});

  }

  
    getAuthors(){
      const booksObservable = new Observable(
        observer => {
          setInterval(
            () => observer.next(this.authors),5000
          )
        }
      )
      return booksObservable;
  
  }
}
