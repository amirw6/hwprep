import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {

  constructor(public postsservice:PostsService, 
    public router:Router, public activeroute:ActivatedRoute,private authservice:AuthService) { }

  title:string;
  author:string;
  id:string;
  isEdit:boolean = false;
  buttonText:string = "Add Post";
  userId:string;


  ngOnInit() {
    this.id = this.activeroute.snapshot.params.id;
    this.authservice.user.subscribe(
      user=> {
        this.userId = user.uid;
        //relevant only for update book
        if(this.id){
          this.isEdit = true;
          this.buttonText = "Update Book";
          this.postsservice.getPost(this.id, this.userId).subscribe(
            book => {
              this.author = book.data().author;
              this.title = book.data().title;
            })
        }
      }
    )
  }



  

  onSubmit(){
    if(this.isEdit){
      this.postsservice.updatePost(this.userId,this.id,this.title,this.author);
      
    }
    else{
    this.postsservice.addPosts(this.userId,this.title,this.author);
    
    }
    this.router.navigate(['posts']);
  }
}

  
