import { Component, OnInit } from '@angular/core';
import {AuthorsService} from './../authors.service'
import { Observable } from 'rxjs';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  //authors: any;
  authors$:Observable<any>;
  test;


  name;

  
  constructor(private authorsservice:AuthorsService) {}
  
  addAuthors(name){
   // this.authors$ + name;
    this.authorsservice.addAuthors(name);
     }

  ngOnInit() {
    this.authors$ = this.authorsservice.getAuthors();
  }

}