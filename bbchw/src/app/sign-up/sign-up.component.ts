import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  
  email:string;
  password:string;

  constructor(private auth:AuthService, private router:Router) { }


  ngOnInit() {

  }

  onSubmit(){

    this.auth.signup(this.email,this.password);
    this.router.navigate(['/books']);

  }

}
