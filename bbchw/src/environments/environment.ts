// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
  apiKey: "AIzaSyASMh5MUPXQgL6ZQH934cZ-lArLN4qTIEU",
    authDomain: "bbchw-a1117.firebaseapp.com",
    databaseURL: "https://bbchw-a1117.firebaseio.com",
    projectId: "bbchw-a1117",
    storageBucket: "bbchw-a1117.appspot.com",
    messagingSenderId: "144530438439",
    appId: "1:144530438439:web:a041ac3b4937f13fb19102",
    measurementId: "G-4GTE8VSTYP"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
